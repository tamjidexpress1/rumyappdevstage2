package com.example.tamjid.rumyapp;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public WebView myWebView = null;
    private ProgressBar pgbar;


    private BluetoothAdapter mBluetoothAdapter;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private boolean mScanning;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 5000;
    private ArrayList<BluetoothDevice> mLeDevices;
    private  deviceListAdapter myadapter;
    private CookieManager cm;
    private final static int REQUEST_ENABLE_BT = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[] {Manifest.permission.ACCESS_COARSE_LOCATION},PERMISSION_REQUEST_COARSE_LOCATION);
        }

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "error_bluetooth_not_supported", Toast.LENGTH_SHORT).show();
            Log.d("tagg","bleadapter is null");
            finish();

        }


        mHandler = new Handler();
       // mLeDevices=new ArrayList<BluetoothDevice>();

        myWebView = (WebView) findViewById(R.id.webView);
        myWebView.setWebViewClient(new MyWebViewClient());
        myWebView.addJavascriptInterface(new WebAppInterface(this), "Android");

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSaveFormData(false);
        //CookieManager.getInstance().setAcceptCookie(true);
        //webSettings.setAppCachePath("/data/data/com.example.tamjid.mywebviewapp/cache");
         //webSettings.setAppCacheEnabled(true);
        //webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);


        myWebView.loadUrl("https://rumytechnologies.com/app/default/index");


        myadapter=new deviceListAdapter();

        final ListView ltView= (ListView) findViewById(R.id.listView);
        ltView.setAdapter(myadapter);
        ltView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final BluetoothDevice device= myadapter.getDevice(position);
               /*StringBuilder sb=new StringBuilder();
                sb.append("{\"device\":[{\"devicename\":\""+device.getName()+"\",\"deviceadress\":\""+device.getAddress()+"\"}]}");
                String str=sb.toString();
                byte[] ar=str.getBytes();
                String test=new String(ar);
                Log.d("tagg",test+"   after converting byte");
                if(str!=null){
                    myWebView.postUrl("https://rumytechnologies.com/app/default/get_rumys",str.getBytes());
                    myWebView.setVisibility(View.VISIBLE);
                }*/
                try{
               /* URL url=new URL("https://rumytechnologies.com/app/default/get_rumys");
                HttpURLConnection con= (HttpURLConnection) url.openConnection();
                con.setDoOutput(true);
                con.setDoInput(false);
                con.setRequestMethod("POST");
                con.connect();*/
                //OutputStream outputStream=con.getOutputStream();
               // OutputStreamWriter outputStreamWriter=new OutputStreamWriter(outputStream);
                    ltView.setVisibility(View.INVISIBLE);
                    myWebView.setVisibility(View.VISIBLE);
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("devicename",device.getName());
                jsonObject.put("deviceaddress",device.getAddress());
                Log.d("tagg",jsonObject.toString());
                //outputStreamWriter.write(URLEncoder.encode(jsonObject.toString(), "UTF-8"));
                //outputStreamWriter.flush();
                //outputStreamWriter.close();
                    String str= new String(jsonObject.toString());

                    myWebView.postUrl("https://rumytechnologies.com/app/default/get_rumys",str.getBytes());


                }
                catch(Exception e){
                    Log.d("tagg",e.getMessage()+"  exception at outputstream");
                }
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
        settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();
        filters = new ArrayList<ScanFilter>();
        if (mLEScanner == null) {
            Log.d("tagg","scanner is null");
        }
         cm= CookieManager.getInstance();
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }
    public class WebAppInterface  {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }


        @JavascriptInterface
        public void showToast(String toast) {


            scanLeDevice(true);

            if(cm.hasCookies()){
                cm.flush();
                Log.d("tagg",cm.getCookie("https://rumytechnologies.com/app/default/index")+"   this is cookie");
            }
            else{
                Log.d("tagg","Cookie not founnd for index");
            }
myWebView.setVisibility(View.INVISIBLE);


        }

    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {


            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    mScanning = false;
                    mLEScanner.stopScan(mScanCallback);

                }

            }, SCAN_PERIOD);
            Log.i("tagg","I'm in scan method");
            mScanning=true;
            mLEScanner.startScan(filters, settings, mScanCallback);
        }
        else {

            mScanning=false;
            mLEScanner.stopScan(mScanCallback);

        }
    }
    private ScanCallback mScanCallback = new ScanCallback() {

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            //Log.i("tagg", String.valueOf(callbackType));
            //Log.i("tagg", result.toString());
           // Log.i("tagg", "I'm in");

            BluetoothDevice device = result.getDevice();
            myadapter.addDevice(device);
            myadapter.notifyDataSetChanged();
        }
    };
private class deviceListAdapter extends BaseAdapter{
    private ArrayList<BluetoothDevice> mLeDevices;
    private LayoutInflater mInflator;

    public deviceListAdapter() {
        super();
        mLeDevices = new ArrayList<BluetoothDevice>();
        mInflator = MainActivity.this.getLayoutInflater();
    }

    public void addDevice(BluetoothDevice device) {
        if(!mLeDevices.contains(device)) {
            mLeDevices.add(device);
        }
    }
    public BluetoothDevice getDevice(int position) {
        return mLeDevices.get(position);
    }
    public void clear() {
        mLeDevices.clear();
    }
    @Override
    public int getCount() {
        return mLeDevices.size();
    }

    @Override
    public Object getItem(int position) {
        return mLeDevices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        // General ListView optimization code.
        if (view == null) {
            view = mInflator.inflate(R.layout.listitem_device, null);
            viewHolder = new ViewHolder();
            viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
            viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        BluetoothDevice device = mLeDevices.get(position);
        final String deviceName = device.getName();
        if (deviceName != null && deviceName.length() > 0)
            viewHolder.deviceName.setText(deviceName);
        else
            viewHolder.deviceName.setText("unknown_device");
        viewHolder.deviceAddress.setText(device.getAddress());

        return view;
    }
}
    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }

}
